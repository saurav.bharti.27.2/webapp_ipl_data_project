

const express = require('express')

const fs = require('fs')

const path = require('path')

const get_data = require('./ipl')

const app = express()

const port  = process.env.PORT || 3000

// calling all the functions
get_data()


app.use(express.static(path.join(__dirname, '..', 'public')))

let html_file_path = path.join(__dirname ,'..', 'public', 'html_files' )

app.get('/', (req, res) => {
    console.log(`${html_file_path}/homepage.html`)
    res.sendFile(`${html_file_path}/homepage.html`)
})


app.get('/totalNoOfMatchPlayedPerYear', (req, res) => {
    
    res.sendFile(`${html_file_path}/totalNoOfMatchPlayedPerYear.html`)
    
})

app.get('/extraRunConcededByEachTeamInYear2016', (req, res) => {

    res.sendFile(`${html_file_path}/extraRunConcededByEachTeamInYear2016.html`)

})

app.get('/numberOfTimesATeamWonTossAndAlsoTheMatch', (req, res) => {

    res.sendFile(`${html_file_path}/numberOfTimesATeamWonTossAndAlsoTheMatch.html`)

})

app.get('/top10EconomicalBowlersIn2015', (req, res)=> {

    res.sendFile(`${html_file_path}/top10EconomicalBowlersIn2015.html`)


})

app.get('/noOfMatchWonPerTeamPerYear', (req, res) => {

    res.sendFile(`${html_file_path}/noOfMatchWonPerTeamPerYear.html`)

})

app.get('/playerWhoHasWonMostPlayerOfTheMatchForEachSeason', (req, res) => {

    res.sendFile(`${html_file_path}/playerWhoHasWonMostPlayerOfTheMatchForEachSeason.html`)

})

app.get('/bowlerWithBestEconomyInSuperOvers', (req, res) => {

    res.sendFile(`${html_file_path}/bowlerWithBestEconomyInSuperOvers.html`)

})

app.get('/highestNumberOfTimesOnePlayerIsDismissedByOtherPlayer', (req, res) => {

    res.sendFile(`${html_file_path}/highestNumberOfTimesOnePlayerIsDismissedByOtherPlayer.html`)

})

app.get('/strikeRateOfABatsmanForEachSeason', (req, res) => {

    res.sendFile(`${html_file_path}/strikeRateOfABatsmanForEachSeason.html`)

} )

app.listen(port, ()=>{
    console.log(`Port is listening at ${port}`)
})