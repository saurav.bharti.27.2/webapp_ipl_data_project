const fs= require('fs-extra')

const {parse} = require('csv-parse/sync')

function get_data(){

    const match_data = new Promise( (resolve, reject) => {
        fs.readFile('src/data/matches.csv', 'utf-8' , (err, data) => {
            if(err){
                reject(err)
            }else{
                
                const parsed_match_data = parse(
                    data,
                    {
                        columns : true,
                        skip_empty_lines: true
                    }
                )

                resolve(parsed_match_data)
            }
        }) 

    })

    const delivery_data = new Promise((resolve, reject) => {
        fs.readFile('src/data/deliveries.csv', 'utf-8', (err, data)=> {
            if(err){
                reject(err)
            }else{
                
                const parsed_delivery_data = parse(
                    data,
                    {
                        columns : true,
                        skip_empty_lines: true
                    }
                )

                resolve(parsed_delivery_data)
            }
        })
    })
    
    Promise.all([match_data, delivery_data]).then((outcome)=> {

        let match_data = outcome[0]

        let delivery_data = outcome[1]

        const problem1_result = total_no_of_match_played_per_year(match_data, delivery_data)
        dump_in_json_file(problem1_result, 'src/public/output/totalNoOfMatchPlayedPerYear.json')


        const problem2_result = no_of_match_won_per_team_per_year(match_data, delivery_data)
        dump_in_json_file(problem2_result, 'src/public/output/noOfMatchWonPerTeamPerYear.json')

        const problem3_result = extra_run_conceded_by_each_team_in_year_2016(match_data, delivery_data)
        dump_in_json_file(problem3_result, 'src/public/output/extraRunConcededByEachTeamInYear2016.json')

        const problem4_result = top_10_economical_bowlers_in_2015(match_data, delivery_data)
        dump_in_json_file(problem4_result, 'src/public/output/top10EconomicalBowlersIn2015.json')

        const problem5_result = number_of_times_a_team_won_toss_and_also_the_match(match_data, delivery_data)
        dump_in_json_file(problem5_result, 'src/public/output/numberOfTimesATeamWonTossAndAlsoTheMatch.json')

        const problem6_result = player_who_has_won_most_player_of_the_match_for_each_season(match_data, delivery_data)
        dump_in_json_file(problem6_result, 'src/public/output/playerWhoHasWonMostPlayerOfTheMatchForEachSeason.json')

        const problem7_result = strike_rate_of_a_batsman_for_each_season(match_data, delivery_data)
        dump_in_json_file(problem7_result, 'src/public/output/strikeRateOfABatsmanForEachSeason.json')

        const problem8_result = highest_number_of_times_one_player_is_dismissed_by_other_player(match_data, delivery_data)
        dump_in_json_file(problem8_result, 'src/public/output/highestNumberOfTimesOnePlayerIsDismissedByOtherPlayer.json')

        const problem9_result = bowler_with_best_economy_in_super_overs(match_data, delivery_data)
        dump_in_json_file(problem9_result, 'src/public/output/bowlerWithBestEconomyInSuperOvers.json')

    }).catch(error => {
        console.error(error)
    })

}

function total_no_of_match_played_per_year(match_data, delivery_data){

    let matches_played_per_year = {}
    
    for( let index=0;index< match_data.length; index++ ){

        let season = match_data[index].season;

        if(!matches_played_per_year[season]){

            matches_played_per_year[season] = 0
            
        }

        matches_played_per_year[season] += 1
    }

    return matches_played_per_year

    
}


function no_of_match_won_per_team_per_year(match_data, delivery_data){
    
        let matches_won_by_per_team_per_year = {}
    
        for(let index=0;index<match_data.length; index++){

            let season = match_data[index].season, team1 = match_data[index].team1, team2 = match_data[index].team2 
    
            let winner = match_data[index].winner;
    
            if(!matches_won_by_per_team_per_year[season]){
                matches_won_by_per_team_per_year[season] = {}
            }
    
            
            if(!matches_won_by_per_team_per_year[season][team1]){
                matches_won_by_per_team_per_year[season][team1] = (winner === team1 ?  1 : 0)
            }else{
                matches_won_by_per_team_per_year[season][team1] += (winner === team1 ? 1 : 0)
            }
    
            if(!matches_won_by_per_team_per_year[season][team2]){
                matches_won_by_per_team_per_year[season][team2] = (winner === team2 ? 1 : 0)
            }else{
                matches_won_by_per_team_per_year[season][team2] += (winner === team2 ? 1 : 0)
            }
            
            
        }
        
        return matches_won_by_per_team_per_year;
           
}


function extra_run_conceded_by_each_team_in_year_2016(match_data, delivery_data){

    
    let by_each_team_extra_run_conceded_in_2016 = {}

    for(let index=0; index<delivery_data.length; index++){

        let match_id = parseInt(delivery_data[index].match_id);

        let season = match_data[match_id-1].season; 

        if(season === '2016'){

            let bowling_team = delivery_data[index].bowling_team
    
            let extra_run_conceded = delivery_data[index].extra_runs
    
            if(!by_each_team_extra_run_conceded_in_2016[bowling_team]){
                by_each_team_extra_run_conceded_in_2016[bowling_team] = 0
            }
    
            if(parseInt(extra_run_conceded)){
                by_each_team_extra_run_conceded_in_2016[bowling_team] += parseInt(extra_run_conceded)
            }
        }

    }
    
    return by_each_team_extra_run_conceded_in_2016;

}

function top_10_economical_bowlers_in_2015(match_data, delivery_data){

    
    let best_10_economical_bowlers_in_2015 = []

    let economy_of_bowlers_in_2015 = {}

    for(let index=0; index< delivery_data.length ; index++){

        let match_id = parseInt(delivery_data[index].match_id);
        
        let season = match_data[match_id - 1].season;
        
        if(season === '2015'){

            let bowler = delivery_data[index].bowler
    
            let total_runs_on_a_ball = parseInt(delivery_data[index].total_runs)

            let is_wide = parseInt(delivery_data[index].wide_runs)
            
            let is_no_ball = parseInt(delivery_data[index].noball_runs);

            let is_bye= parseInt(delivery_data[index].bye_runs)
        
            let is_legbye= parseInt(delivery_data[index].legbye_runs)

            if(!economy_of_bowlers_in_2015[bowler]){

                economy_of_bowlers_in_2015[bowler] = [0, 0]

            }
            let [total_ball_bowled_by_him, runs_conceded] = economy_of_bowlers_in_2015[bowler]

            economy_of_bowlers_in_2015[bowler] = [ total_ball_bowled_by_him + ( (is_wide || is_no_ball) ? 0 : 1) , runs_conceded + ((is_bye || is_legbye ) ? 0: total_runs_on_a_ball) ]

        }

    }

    // Not to count balls - Wide, No ball,

    let bowlers_by_their_economy = []

    for( let bowler in economy_of_bowlers_in_2015){

        bowlers_by_their_economy.push([ (economy_of_bowlers_in_2015[bowler][1]/economy_of_bowlers_in_2015[bowler][0] * 6) , bowler ])
    }

    bowlers_by_their_economy = bowlers_by_their_economy.sort((bowler_a, bowler_b) => (bowler_a[0] - bowler_b[0]))

    for(let index=0; index<10; index++){

        best_10_economical_bowlers_in_2015.push(bowlers_by_their_economy[index])

    }

    return best_10_economical_bowlers_in_2015 

}

function number_of_times_a_team_won_toss_and_also_the_match(match_data, delivery_data){

    

    let total_number_of_times_a_team_won_toss_and_also_the_match = {};

    for(let index=0; index<match_data.length; index++){

        const team_who_won_the_toss = match_data[index].toss_winner

        const team_who_won_the_match = match_data[index].winner

        if(team_who_won_the_match === team_who_won_the_toss){

            if(!total_number_of_times_a_team_won_toss_and_also_the_match[team_who_won_the_toss]){

                total_number_of_times_a_team_won_toss_and_also_the_match[team_who_won_the_toss] = 0

            }
            total_number_of_times_a_team_won_toss_and_also_the_match[team_who_won_the_toss] += 1

        }

    }

    return total_number_of_times_a_team_won_toss_and_also_the_match

}


function player_who_has_won_most_player_of_the_match_for_each_season(match_data, delivery_data){

    let most_player_of_the_match_winners_by_each_season = {}

    frequency_of_pom_by_each_season = {}

    for(let index=0; index< match_data.length; index++){

        let season = match_data[index].season

        let player_of_match = match_data[index].player_of_match


        if(!frequency_of_pom_by_each_season[season]){

            frequency_of_pom_by_each_season[season] = {}

        }

        if( !frequency_of_pom_by_each_season[season][player_of_match] ){

            frequency_of_pom_by_each_season[season][player_of_match] = 1

        }else{

            frequency_of_pom_by_each_season[season][player_of_match] += 1

        }

    }

    for( const season in frequency_of_pom_by_each_season){

        let maximum= -100000000

        let most_player_of_match_winners_for_the_season = []

        for( const good_players in frequency_of_pom_by_each_season[season] ){
            
            if(frequency_of_pom_by_each_season[season][good_players] > maximum){

                maximum = frequency_of_pom_by_each_season[season][good_players]
                
                most_player_of_match_winners_for_the_season = []

                most_player_of_match_winners_for_the_season.push( [good_players, maximum] )

            }
            else if( frequency_of_pom_by_each_season[season][good_players] == maximum ){

                most_player_of_match_winners_for_the_season.push( [good_players, maximum] )

            }
        }
        
        most_player_of_the_match_winners_by_each_season[season] = most_player_of_match_winners_for_the_season

    }

    return most_player_of_the_match_winners_by_each_season;
 
    
}

function strike_rate_of_a_batsman_for_each_season(match_data, delivery_data){

        let strike_rate_per_season_for_a_batsman = {}

        let balls_played_vs_scored_for_each_batsman_per_season = {}

        for( let index = 0; index<delivery_data.length; index++){
            
            let match_id = parseInt(delivery_data[index].match_id)

            let season = match_data[match_id - 1].season

            let batsman = delivery_data[index].batsman

            if(!balls_played_vs_scored_for_each_batsman_per_season[batsman]){

                balls_played_vs_scored_for_each_batsman_per_season[batsman] = {}

            }

            if( !balls_played_vs_scored_for_each_batsman_per_season[batsman][season]){

                balls_played_vs_scored_for_each_batsman_per_season[batsman][season] = [0, 0]

            }

            let runs_scored_by_batsman = parseInt(delivery_data[index].batsman_runs)

            let is_wide = parseInt(delivery_data[index].wide_runs)

            let is_no_ball = parseInt(delivery_data[index].noball_runs);

            let [balls_played_by_the_batsman, total_run_of_his] = balls_played_vs_scored_for_each_batsman_per_season[batsman][season]

            balls_played_vs_scored_for_each_batsman_per_season[batsman][season] = [ ((is_wide || is_no_ball) ? 0 : 1) + balls_played_by_the_batsman , runs_scored_by_batsman + total_run_of_his  ];

        }


        for(const batsman in balls_played_vs_scored_for_each_batsman_per_season){

            for(const season_played_by_him in balls_played_vs_scored_for_each_batsman_per_season[batsman]){

                if(!strike_rate_per_season_for_a_batsman[batsman]){

                    strike_rate_per_season_for_a_batsman[batsman] = {}

                }

                if(!strike_rate_per_season_for_a_batsman[batsman][season_played_by_him]){


                    strike_rate_per_season_for_a_batsman[batsman][season_played_by_him] = ( balls_played_vs_scored_for_each_batsman_per_season[batsman][season_played_by_him][1]/balls_played_vs_scored_for_each_batsman_per_season[batsman][season_played_by_him][0] ) * 100 
                }
            }
        }

        return strike_rate_per_season_for_a_batsman

}

function highest_number_of_times_one_player_is_dismissed_by_other_player(match_data, delivery_data){
       
    let all_bowlers_who_dismissed_a_batsman = {}

    let most_times_one_bowler_got_out_one_batsman = {}

    for (let index=0; index < delivery_data.length; index++){

        let is_dismissed = delivery_data[index].player_dismissed

        let dismissal_type = delivery_data[index].dismissal_kind

        let batsman= delivery_data[index].batsman

        let bowler = delivery_data[index].bowler

        if(is_dismissed != ''){

            if(dismissal_type !== 'run out'){
    
                if(!all_bowlers_who_dismissed_a_batsman[batsman]){
    
                    all_bowlers_who_dismissed_a_batsman[batsman] = {}
                }
    
                if(!all_bowlers_who_dismissed_a_batsman[batsman][bowler]){
                    all_bowlers_who_dismissed_a_batsman[batsman][bowler] = 0
                }
    
                all_bowlers_who_dismissed_a_batsman[batsman][bowler] += 1
    
            }

        }

    }


    let maximum = -10000000

    for(const batsman in all_bowlers_who_dismissed_a_batsman){

        for(const bowlers in all_bowlers_who_dismissed_a_batsman[batsman]){

            if(all_bowlers_who_dismissed_a_batsman[batsman][bowlers] > maximum){

                maximum = all_bowlers_who_dismissed_a_batsman[batsman][bowlers]

                most_times_one_bowler_got_out_one_batsman = [ maximum, batsman, bowlers ]

            }
        }

    }

    return most_times_one_bowler_got_out_one_batsman


}

function bowler_with_best_economy_in_super_overs(match_data, delivery_data){

    let bowler_with_minimum_economy_in_super_overs;

    let bowler_with_super_overs_ball_and_runs = {}

    for(let index =0; index< delivery_data.length; index++){
        
        let bowler = delivery_data[index].bowler

        let super_over = parseInt(delivery_data[index].is_super_over)

        let is_wide = parseInt(delivery_data[index].wide_runs)

        let is_no_ball = parseInt(delivery_data[index].noball_runs)

        let total_runs = parseInt(delivery_data[index].total_runs)

        let is_bye= parseInt(delivery_data[index].bye_runs)
            
        let is_legbye= parseInt(delivery_data[index].legbye_runs)

        if(super_over){

            if(!bowler_with_super_overs_ball_and_runs[bowler]){

                bowler_with_super_overs_ball_and_runs[bowler] = [0, 0]

            }

            const [balls_bowled_by_him, runs_conceded] = bowler_with_super_overs_ball_and_runs[bowler]

            bowler_with_super_overs_ball_and_runs[bowler] = [ ((is_wide || is_no_ball) ? 0 : 1) + balls_bowled_by_him, runs_conceded + ((is_bye || is_legbye) ? 0 : total_runs) ]

        }
    }

    let minimum = 10000000

    for(const bowler in bowler_with_super_overs_ball_and_runs){

        
        let his_economy = (bowler_with_super_overs_ball_and_runs[bowler][1] / bowler_with_super_overs_ball_and_runs[bowler][0] ) * 6

        if(his_economy < minimum){

            minimum = his_economy;

            bowler_with_minimum_economy_in_super_overs = bowler

        }
    }

    return {bowler_with_minimum_economy_in_super_overs, his_economy : minimum  }


}



function dump_in_json_file(result, address){

    try{
        
        fs.outputJson( address, result, {spaces : 2}).
        then(() => fs.readJson(address)).
        then().
        catch(err => {console.error(err)})

    }
    catch(err){

        console.log(err.stack)

    }
    
}


module.exports = get_data