
async function fetch_data_strike_rate_of_a_batsman_for_each_season(){

    let data_strike_rate_of_a_batsman_for_each_season = await fetch('../output/strikeRateOfABatsmanForEachSeason.json')
    
    let strike_rate_of_a_batsman_for_each_season_json = await data_strike_rate_of_a_batsman_for_each_season.json()

    
    Object.entries(strike_rate_of_a_batsman_for_each_season_json).forEach((batsman, index) => {
        

        const containerId = `container${index}`

        const container = document.createElement('div')

        container.id = containerId

        document.getElementById('chart1').appendChild(container)

        let team_series =  []

        team_series = Object.entries(batsman[1]).map((each_year) => {

            let rounded_strike_rate = Number(each_year[1].toFixed(2))
            return {
                name : each_year[0],
                data : [rounded_strike_rate]
            }
        })
       

        Highcharts.chart(containerId, {

            title: {
                text : "Each batsman strike rate each year"
            },
            credits: {
                enabled : false
            },
            chart: {
                type : "column"
            },
            xAxis : {
                categories : [batsman[0]]
            },
            yAxis: {
                title : {
                    text : "Strike rate per year"
                }
            },
    
            series: team_series
        })


    }) 
    
    
    
}

fetch_data_strike_rate_of_a_batsman_for_each_season();

