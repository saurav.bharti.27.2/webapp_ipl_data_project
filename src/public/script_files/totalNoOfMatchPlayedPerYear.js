
async function fetch_data_total_match_per_year(){
    let total_match_data = await fetch('../output/totalNoOfMatchPlayedPerYear.json')
    
    let total_match_json = await total_match_data.json()

    let team_series= Object.entries(total_match_json).map((each_team) => {
        return {
            name : each_team[0],
            data : [each_team[1]]
        }
    });
    
    Highcharts.chart('chart1', {

        title: {
            text : "Matches Played per Year"
        },
        credits: {
            enabled : false
        },
        chart: {
            type : "column"
        },
        xAxis : {
            categories : ['Years']
        },
        yAxis: {
            title : {
                text : "Matches played per Year"
            }
        },

        series: team_series
    })
    
}

fetch_data_total_match_per_year();


