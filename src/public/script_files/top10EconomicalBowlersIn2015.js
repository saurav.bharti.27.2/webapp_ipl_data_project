
async function fetch_data_top_10_economical_bowlers_in_2015(){

    let data_top_10_economical_bowlers_in_2015 = await fetch('../output/top10EconomicalBowlersIn2015.json')
    
    let top_10_economical_bowlers_in_2015_json = await data_top_10_economical_bowlers_in_2015.json()

    let team_series= Object.entries(top_10_economical_bowlers_in_2015_json).map((each_team) => {

        let rounded_economy = Number(each_team[1][0].toFixed(2));
        return {
            name : each_team[1][1],
            data : [rounded_economy]
        }
    });
    
    Highcharts.chart('chart1', {

        title: {
            text : "Top 10 economical bowlers in 2015"
        },
        credits: {
            enabled : false
        },
        chart: {
            type : "column"
        },
        xAxis : {
            categories : ['Bowlers ']
        },
        yAxis: {
            title : {
                text : "Economy"
            }
        },

        series: team_series
    })
    
}

fetch_data_top_10_economical_bowlers_in_2015();