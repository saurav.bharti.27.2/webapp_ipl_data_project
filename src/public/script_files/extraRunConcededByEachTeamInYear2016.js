
async function fetch_data_extra_runs(){
    let extra_runs = await fetch('../output/extraRunConcededByEachTeamInYear2016.json')
    
    let extra_runs_json = await extra_runs.json()

    let team_series= Object.entries(extra_runs_json).map((each_team) => {
        return {
            name : each_team[0],
            data : [each_team[1]]
        }
    });
    
    Highcharts.chart('chart1', {

        title: {
            text : "Extra runs conceded per team in 2016"
        },
        credits: {
            enabled : false
        },
        chart: {
            type : "column"
        },
        xAxis : {
            categories : ['Extra runs conceded per team in 2016']
        },
        yAxis: {
            title : {
                text : "Number of matches won"
            }
        },

        series: team_series
    })
    
}

fetch_data_extra_runs();


