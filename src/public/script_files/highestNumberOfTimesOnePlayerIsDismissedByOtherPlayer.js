
async function fetch_data_highest_number_of_times_one_player_dismissed_by_other_player(){

    let data_highest_number_of_times_one_player_dismissed_by_other_player = await fetch('../output/highestNumberOfTimesOnePlayerIsDismissedByOtherPlayer.json')
    
    let highest_number_of_times_one_player_dismissed_by_other_player_json = await data_highest_number_of_times_one_player_dismissed_by_other_player.json()

    let team_series= []

    let obj = {

        name : [highest_number_of_times_one_player_dismissed_by_other_player_json[1], highest_number_of_times_one_player_dismissed_by_other_player_json[2]],

        data : [Number(highest_number_of_times_one_player_dismissed_by_other_player_json[0])]
    }

    team_series.push(obj)
    
    Highcharts.chart('chart1', {

        title: {
            text : "Highest Number of times one player is dismissed by another player "
        },
        credits: {
            enabled : false
        },
        chart: {
            type : "column"
        },
        xAxis : {
            categories : ["Bowler - " + highest_number_of_times_one_player_dismissed_by_other_player_json[2]]
        },
        yAxis: {
            title : {
                text : "Batsman - " + [highest_number_of_times_one_player_dismissed_by_other_player_json[1]]
            }
        },

        series: team_series
    })
    
}

fetch_data_highest_number_of_times_one_player_dismissed_by_other_player();