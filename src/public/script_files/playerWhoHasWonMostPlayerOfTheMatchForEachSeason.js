
async function fetch_data_player_who_has_won_most_player_of_match_each_year(){

    let data_player_who_has_won_most_player_of_match_each_year= await fetch('../output/playerWhoHasWonMostPlayerOfTheMatchForEachSeason.json')
    
    let player_who_has_won_most_player_of_match_each_year_json = await data_player_who_has_won_most_player_of_match_each_year.json()

    let team_series= []

    Object.entries(player_who_has_won_most_player_of_match_each_year_json).map((each_season) => {

        team_series.push({
            name : each_season[1].map((player) => {
                return player
            }),
            data : [each_season[1][0]]
        })
    });

    
    Highcharts.chart('chart1', {

        title: {
            text : "Most player of the match winner each year"
        },
        credits: {
            enabled : false
        },
        chart: {
            type : "column"
        },
        xAxis : {
            categories : ['Players ']
        },
        yAxis: {
            title : {
                text : "Number of Player of the Match won per Season"
            }
        },

        series: team_series
    })

}


fetch_data_player_who_has_won_most_player_of_match_each_year()