//noOfMatchWonPerTeamPerYear

async function fetch_data_no_of_match_won_per_team_per_year(){

    let data_no_of_match_won_per_team_per_year = await fetch('../output/noOfMatchWonPerTeamPerYear.json')
    
    let no_of_match_won_per_team_per_year_json = await data_no_of_match_won_per_team_per_year.json()

    Object.keys(no_of_match_won_per_team_per_year_json).forEach((year) => {

        const containerId = `charts${year}`

        const chartData = no_of_match_won_per_team_per_year_json[year]

        const container = document.createElement('div')

        container.id = containerId
        
        document.getElementById("chart1").appendChild(container)

        const series_data = Object.entries(no_of_match_won_per_team_per_year_json[year]).map((team_and_match) => {

            return {
                name:  team_and_match[0],
                data: [team_and_match[1]]

            }

        })

        Highcharts.chart(containerId, {
            chart: {
                type: 'column'
            },
            title: {
                text: `Matches Won by Each Team in ${year}`
            },
            xAxis: {
                categories: ['Teams']
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Matches Won'
                }
            },
            series: series_data
        });


    })

}

fetch_data_no_of_match_won_per_team_per_year();