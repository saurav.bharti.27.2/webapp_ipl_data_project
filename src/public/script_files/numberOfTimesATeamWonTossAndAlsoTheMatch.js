// numberOfTimesATeamWonTossAndAlsoTheMatch


async function fetch_data_no_of_times_team_won_toss_and_match(){

    let data_toss_and_match_both_won = await fetch('../output/numberOfTimesATeamWonTossAndAlsoTheMatch.json')
    
    let toss_and_match_both_won_json = await data_toss_and_match_both_won.json()

    let team_series= Object.entries(toss_and_match_both_won_json).map((each_team) => {
        return {
            name : each_team[0],
            data : [each_team[1]]
        }
    });
    
    Highcharts.chart('chart1', {

        title: {
            text : "Number of times a team won the toss and also match"
        },
        credits: {
            enabled : false
        },
        chart: {
            type : "column"
        },
        xAxis : {
            categories : ['Teams ']
        },
        yAxis: {
            title : {
                text : "Number of matches as well as toss won"
            }
        },

        series: team_series
    })
    
}

fetch_data_no_of_times_team_won_toss_and_match();