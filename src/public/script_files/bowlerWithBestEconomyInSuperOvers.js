// bowlerWithBestEconomyInSuperOvers

async function fetch_data_bowler_with_best_economy_in_super_overs(){

    let data_bowler_with_best_economy_in_super_overs = await fetch('../output/bowlerWithBestEconomyInSuperOvers.json')
    
    let bowler_with_best_economy_in_super_overs_json = await data_bowler_with_best_economy_in_super_overs.json()

    let team_series= []
    let obj = {
        name : bowler_with_best_economy_in_super_overs_json.bowler_with_minimum_economy_in_super_overs,
        data : [Number(bowler_with_best_economy_in_super_overs_json.his_economy)]
    }

    team_series.push(obj)
    
    Highcharts.chart('chart1', {

        title: {
            text : "Bowler with best economy in super overs"
        },
        credits: {
            enabled : false
        },
        chart: {
            type : "column"
        },
        xAxis : {
            categories : ['Bowler']
        },
        yAxis: {
            title : {
                text : "Economy in super overs"
            }
        },

        series: team_series
    })
    
}

fetch_data_bowler_with_best_economy_in_super_overs();